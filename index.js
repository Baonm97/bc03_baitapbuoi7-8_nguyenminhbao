var numberArr = [];
function inKetQua() {
  console.log("yes");
  var numberValue = document.getElementById("number").value * 1;
  numberArr.push(numberValue);
  var noi = numberArr.join(" , ");
  var soChanArr = [];
  var tongSoDuong = 0;
  var soDuongArr = [];
  var soDuong = 0;
  var soDuongArr = [];
  var minSoDuong = 0;
  var lastSoChan = 0;
  /**bài 1 */
  numberArr.forEach(function (item) {
    if (item > 0) {
      tongSoDuong += item;
    }
  });
  /**bài 2 */
  numberArr.forEach(function (item) {
    if (item > 0) {
      soDuongArr.push(item);
      soDuong = soDuongArr.length;
    }
  });
  //   /**bài 3 */
  /**cách 1 */
  //var numberArrs = [];
  // numberArr.forEach(function (item) {
  //   numberArrs.push(item);
  // });
  // // var newArray2 = numberArrs.sort(function (a, b) {
  // //   return a - b;
  // // });
  // // minNumber = newArray2[0];
  /**cach 2 */
  var minNumber = numberArr[0];
  for (var i = 1; i < numberArr.length; i++) {
    if (minNumber > numberArr[i]) {
      minNumber = numberArr[i];
    }
  }
  //   /**bài 4 */
  /**cách 1 */
  // var soDuongArrS = [];
  // soDuongArr.forEach(function (item) {
  //   soDuongArrS.push(item);
  // });
  // if (soDuongArrS.length > 0) {
  //   soDuongArrS.sort(function (a, b) {
  //     return a - b;
  //   });
  //   minSoDuong = soDuongArrS[0];
  // } else {
  //   minSoDuong = `khong co so duong`;
  // }
  /**cách 2 */
  var minSoDuong = soDuongArr[0];
  if (soDuongArr.length > 0) {
    for (var i = 0; i < soDuongArr.length; i++) {
      if (minSoDuong > soDuongArr[i]) {
        minSoDuong = soDuongArr[i];
      }
    }
  } else {
    minSoDuong = "Không có số dương nào ";
  }
  /**bài 5 */
  numberArr.forEach(function (item) {
    if (item % 2 === 0) {
      soChanArr.push(item);
    }
  });
  if (soChanArr.length - 1 >= 0) {
    lastSoChan = soChanArr[soChanArr.length - 1];
  } else {
    lastSoChan = -1;
  }
  document.getElementById("ketQua").innerHTML = ` 
  <div>Các số đã thêm: ${noi}</div>
    <div>Tổng các số dương trong mảng: ${tongSoDuong}</div>
    <div>Đếm số dương có trong mảng: ${soDuong}</div>
    <div>Tìm số nhỏ nhất trong mảng: ${minNumber}</div>
    <div>Số dương nhỏ nhất trong mảng: ${minSoDuong}</div>
    <div>Số chẵn cuối cùng của mảng(nếu không trả về -1): ${lastSoChan}</div>`;
  document.getElementById("ketQua").style.visibility = "visible";
  document.getElementById("ketQua").style.opacity = "1";
  document.getElementById("ketQua").style.borderRadius = "20px";
  document.getElementById("ketQua").style.color = "white";
  document.getElementById("ketQua").style.display = "block";
}
/**Bài 6 */
function inKetQua2() {
  var indexFirst = document.getElementById("vitri_1").value * 1;
  var indexSecond = document.getElementById("vitri_2").value * 1;
  var tmp = numberArr[indexFirst];
  numberArr[indexFirst] = numberArr[indexSecond];
  numberArr[indexSecond] = tmp;
  console.log(numberArr);
  document.getElementById("ketQua2").innerHTML = `${numberArr}`;
}
/**Bài 7 */
function inKetQua3() {
  var sapXep = [];
  numberArr.forEach(function (item) {
    sapXep.push(item);
    sapXep.sort(function (a, b) {
      return a - b;
    });
    sapXep.join(", ");
  });
  document.getElementById("ketQua3").innerHTML = `${sapXep}`;
}
/**Bài 8 */
function inKetQua4() {
  var firstPrime = 0;
  for (var i = 0; i < numberArr.length; i++) {
    let flag = 0;
    if (numberArr[i] < 2) {
      flag = 1;
    }
    for (var j = 2; j < numberArr[i]; j++) {
      if (numberArr[i] % j == 0) {
        flag = 1;
        break;
      }
    }
    if (flag == 1) {
      firstPrime = -1;
    } else {
      firstPrime = numberArr[i];
      break;
    }
  }
  document.getElementById("ketQua4").innerHTML = `${firstPrime}`;
}
/**Bài 9*/
var realNumberArr = [];
function inKetQua5() {
  var demSoNguyen = 0;
  var numValue = document.getElementById("number_2").value * 1;
  realNumberArr.push(numValue);
  // for (i = 0; i < realNumberArr.length; i++) {
  //   let check = Number.isInteger(realNumberArr[i]);
  //   if (check == true) {
  //     demSoNguyen += 1;
  //   }
  // }
  var noichuoi = realNumberArr.join(" , ");
  realNumberArr.forEach(function (item) {
    if (Number.isInteger(item)) {
      return (demSoNguyen += 1);
    }
  });
  document.getElementById(
    "ketQua5"
  ).innerHTML = `<div>Các số đã thêm: ${noichuoi}</div>
  <div>Dem So nguyen: ${demSoNguyen}</div>`;
}
/**Bai 10 */
function inKetQua6() {
  console.log("yess");
  var soDuongArr = [];
  var soAmArr = [];
  numberArr.forEach(function (item) {
    if (item > 0) {
      soDuongArr.push(item);
    } else if (item < 0) {
      soAmArr.push(item);
    }
  });
  if (soDuongArr.length > soAmArr.length) {
    document.getElementById("ketQua6").innerHTML = `So duong nhieu hon`;
  } else if (soDuongArr.length < soAmArr.length) {
    document.getElementById("ketQua6").innerHTML = `So am nhieu hon`;
  } else {
    document.getElementById("ketQua6").innerHTML = `So duong = so am`;
  }
}
